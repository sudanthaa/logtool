LogTool
=======

Tool for collecting logs, which makes logging issues easier, organized and informative.
Log tool can..
 
* Easily access existing XShell session files
* List log/corefiles and configurations in UI for easy selection
* Create jira tickets and add description comments.
* Capture screenshots mark them and attach to JIRA tickets.
* Do custom log actions.
* Enable easy access for log analyzers


![main-interface.png](images/main-interface.png)



## Use the XShell files ##
![xshell-logins.png](images/xshell-logins.png)



## Create JIRA Tickets ##
![jira-issue.png](images/jira-issue.png)



## Add JIRA Comments ##
Add jira comment with descriptive information of the log location

![jira-log-comment.png](images/jira-log-comment.png)



## Take Screenshots and Mark them ##
Tool to create screenshots and mark them with arrows squares or write on it.

![screenshot-marking.png](images/screenshot-marking.png)


## Custom log Collect action ##
This is a very powerful feature where one can write a bash script, make it single line and add as a custom command.
Environment variables set by Logtool enables to address log location.
eg:

```
#!bash

scp envvar.txt $logenvsshcon:$logenvpath
```

![custom-action.png](images/custom-action.png)


## Easy access for the viewers of the Log ##
![open-log-in-xshell.png](images/open-log-in-xshell.png)